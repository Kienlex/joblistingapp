package com.example.kienlexyap.joblistingapp.model;

import java.util.Calendar;

/**
 * Created by kienlexyap on 05/10/2016.
 */

public class EmployeeWorkingExperience {

    private int mUserId; //this share the same ID under Employee, how to deal with this?
    private int mWorkingExperienceId; //int or long
    private Calendar mCreatedDate; //hmm, setting private @ employee making these not accessible?
    private Calendar mUpdatedDate; // is it right to use Calendar at the first place?
    private Calendar mJoinedDate;
    private Calendar mLeftDate;
    private String mCompanyName;
    private int mIndustryID; //int or long?
    private String mJobPosition; // job position = manager (management), ceo (top management), clerk and etc
    private int mJobPositionID; // id for each of the roll (top management, mid , entry, graduates, clerical, cleaner? and etc)


    //getter and setter not done
}
