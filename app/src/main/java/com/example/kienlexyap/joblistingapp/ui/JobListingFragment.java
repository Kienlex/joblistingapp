package com.example.kienlexyap.joblistingapp.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.VolleyError;
import com.example.kienlexyap.joblistingapp.R;
import com.example.kienlexyap.joblistingapp.RestAPIClient;
import com.example.kienlexyap.joblistingapp.model.Job;

import java.util.ArrayList;
import java.util.List;


public class JobListingFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String Tag = "JobListingFragment";

    private Button mEditProfileButton;
    private Button mSearchButton;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private JobListingAdapter mAdapter;
    private List<Job> mJobList = new ArrayList<>();

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_joblisting, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_jobListing_swipeRefreshLayout);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);


        return rootView;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        this.mEditProfileButton = (Button) view.findViewById(R.id.fragment_jobListing_edit_profile_button);
        this.mSearchButton = (Button) view.findViewById(R.id.fragment_jobListing_search_button);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        RestAPIClient.sharedInstance(getContext()).getJobListings(new RestAPIClient.OnGetJobListingCompletedListener() {
            @Override
            public void onComplete(@Nullable List<Job> jobs, @Nullable VolleyError error) {
                mAdapter.setJobList(jobs);
                mAdapter.notifyDataSetChanged();

            }
        });


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new JobListingAdapter(mJobList);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
//        prepareJobData();

   /*     this.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JobListingFragment.this.showJobListingFragment();
            }
        });
   */
        this.mSearchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                JobListingFragment.this.showJobSearchFragment();
            }
        });

        this.mEditProfileButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                JobListingFragment.this.showProfileDetailsFragment();
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();

        Log.d(Tag,"on resume");
    }

    @Override
    public void onPause(){
        super.onPause();

        Log.d(Tag,"on Pause");
    }

    public void showJobSearchFragment(){
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_vg_container, new JobSearchFragment())
                .addToBackStack("Search")
                .commit();
    }

    public void showProfileDetailsFragment(){
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_vg_container, new ProfileDetailsFragment())
                .addToBackStack("Search")
                .commit();
    }
// hardcode portion
//    private void prepareJobData() {
//        Job job = new Job("Toilet Cleaner", "Public Bank", "Clean the damn toilet", 5000);
//        mJobList.add(job);
//
//        job = new Job("Manager", "Rhb Bank", "Look like busy", 5000);
//        mJobList.add(job);
//
//        job = new Job("Graphic Designer", "Random firm", "Draw flower", 7000);
//        mJobList.add(job);
//
//        job = new Job("Supervisor", "Sale Gallery", "Look See Look See", 50000);
//        mJobList.add(job);
//
//        job = new Job("PM assistant", "Someone's office", "Manage donation", 5000000);
//        mJobList.add(job);
//
//        job = new Job("Shopping Kaki", "Someone's wife", "carry handbag and goods", 1000000);
//        mJobList.add(job);
//
//
//        mAdapter.notifyDataSetChanged();
//    }

    @Override
    public void onRefresh() {

        RestAPIClient.sharedInstance(getContext())
                .getJobListings(new RestAPIClient.OnGetJobListingCompletedListener() {
                    @Override
                    public void onComplete(@Nullable List<Job> jobs, @Nullable VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        mAdapter.setJobList(jobs);
                        mAdapter.notifyDataSetChanged();
                    }
                });


    }
}
