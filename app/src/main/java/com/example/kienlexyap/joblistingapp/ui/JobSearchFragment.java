package com.example.kienlexyap.joblistingapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.kienlexyap.joblistingapp.R;

/**
 * Created by kienlexyap on 07/09/2016.
 */
public class JobSearchFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {
        private static final String Tag ="Checking";
        private Button mUpdateAndFindButton;
        private SeekBar mSeekBar;
        private Spinner mSpinner;
        private TextView textProgress;
        private TextView textAction;

        @Nullable
        @Override

        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_jobsearch, container, false);

        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

            this.mSeekBar = (SeekBar) view.findViewById(R.id.fragment_jobSearch_SeekBar);
            this.mUpdateAndFindButton = (Button) view.findViewById(R.id.fragment_jobSearch_UpdateAndFindButton);
            this.mSpinner = (Spinner) view.findViewById(R.id.jobSearchSpinner);
            // make text label for progress value
            textProgress = (TextView) view.findViewById(R.id.fragment_jobSearch_Salary);
            // make text label for action
            textAction = (TextView) view.findViewById(R.id.fragment_jobSearch_Test);

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.myArray, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinner.setPrompt("Select your preferred location!");


            mSeekBar.setOnSeekBarChangeListener(this);
            mSeekBar.setMax(40000);

            mSpinner.setAdapter(
                    new JobSearchSpinnerAdapter(
                            adapter,
                            R.layout.contact_spinner_row_nothing_selected,
                            // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                            getContext()));

   /*     this.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JobListingFragment.this.showJobListingFragment();
            }
        });
   */
            this.mUpdateAndFindButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    JobSearchFragment.this.showJobListingFragment();
                }
            });
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onResume(){
            super.onResume();

            Log.d(Tag,"Check 1");
        }

        @Override
        public void onPause(){
            super.onPause();

            Log.d(Tag,"Check 2");
        }

        public void showJobListingFragment(){
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.activity_main_vg_container, new JobListingFragment())
                    .addToBackStack("Search")
                    .commit();
        }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        textProgress.setText("RM: "+progress);
        // change action text label to changing
        textAction.setText("");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
// TODO Auto-generated method stub
        textAction.setText("starting to track touch");

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        seekBar.setSecondaryProgress(seekBar.getProgress());
        textAction.setText("Expected Salary is set at " + textProgress.getText());
    }
}


