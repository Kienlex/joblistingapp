package com.example.kienlexyap.joblistingapp;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kienlexyap on 10/10/2016.
 */

public class BackendlessJsonObjectRequest extends JsonObjectRequest {
    public BackendlessJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public BackendlessJsonObjectRequest(String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>(super.getHeaders());

        headers.put("application-id", "E2EB0887-6C46-A49D-FF3E-32D487F66300");
        headers.put("secret-key", "5C013BC4-CC1C-3455-FF66-DF4DF5C81B00");
        headers.put("application-type","REST");
        headers.put("Content-Type", "application/json");

        return headers;
    }
}
