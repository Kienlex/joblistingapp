package com.example.kienlexyap.joblistingapp.model;

import java.util.Calendar;

/**
 * Created by kienlexyap on 05/10/2016.
 */

public class Company {

    private int mCompanyId; //need to remove SetID
    private String mCompanyName;
    private String mCompanyDescription;
    private String mCompanyAddress;
    private Calendar mJoinedDate;
    private String mIndustry;

    public int getIndustryId() {
        return mIndustryId;
    }

    public void setIndustryId(int industryId) {
        mIndustryId = industryId;
    }

    private int mIndustryId;

    public int getCompanyId() {
        return mCompanyId;
    }

    public void setCompanyId(int companyId) {
        mCompanyId = companyId;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getCompanyDescription() {
        return mCompanyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        mCompanyDescription = companyDescription;
    }

    public String getCompanyAddress() {
        return mCompanyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        mCompanyAddress = companyAddress;
    }

    public Calendar getJoinedDate() {
        return mJoinedDate;
    }

    public void setJoinedDate(Calendar joinedDate) {
        mJoinedDate = joinedDate;
    }

    public String getIndustry() {
        return mIndustry;
    }

    public void setIndustry(String industry) {
        mIndustry = industry;
    }

}
