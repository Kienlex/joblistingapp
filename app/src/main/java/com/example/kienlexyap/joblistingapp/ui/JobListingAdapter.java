package com.example.kienlexyap.joblistingapp.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kienlexyap.joblistingapp.R;
import com.example.kienlexyap.joblistingapp.model.Job;

import java.util.List;

/**
 * Created by kienlexyap on 05/10/2016.
 */

public class JobListingAdapter extends RecyclerView.Adapter<JobListingAdapter.MyViewHolder> {

    private List<Job> mJobList;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView mJobName, mCompanyName, mJobDescription, mSalaryRange;

        public MyViewHolder(View itemView) {
            super(itemView);
            mJobName = (TextView) itemView.findViewById(R.id.jobName);
            mCompanyName = (TextView) itemView.findViewById(R.id.companyName);
            mJobDescription = (TextView) itemView.findViewById(R.id.jobDescription);
            mSalaryRange = (TextView) itemView.findViewById(R.id.salaryRange);

        }

    }


    public JobListingAdapter(List<Job> mJobList) {
        this.mJobList = mJobList;
    }
    public void setJobList(List<Job> jobList) {
        mJobList.clear();
        mJobList.addAll(jobList);

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(JobListingAdapter.MyViewHolder holder, int position) {
        Job job = mJobList.get(position);
        holder.mJobName.setText(job.getJobName());
        holder.mCompanyName.setText(job.getCompanyName());
        holder.mJobDescription.setText(job.getJobDescription());
        holder.mSalaryRange.setText("RM"+Integer.toString(job.getSalaryRange()));

    }

    @Override
    public int getItemCount() {
        return mJobList.size();
    }



}

