package com.example.kienlexyap.joblistingapp.model;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by kienlexyap on 05/10/2016.
 */

public class Job {

    private int mJobId;
    private String mJobName; //add this?
    private Calendar mCreatedDate;
    private Calendar mClosedDate;
    private int mCompanyId;
    private String mCompanyName; // add this?
    private String mJobDescription;
    private int mSalaryRange; // why salary range? but not salary alone itself?

//    public Job(String JobName, String CompanyName , String JobDescription , int SalaryRange ){
//        mJobName = JobName;
//        mCompanyName = CompanyName;
////        this.mCreatedDate =mCreatedDate; //do not deal with calendar at the moment
////        this.mClosedDate =mClosedDate; // do not mess with calendar for the timebeing,
//        mSalaryRange = SalaryRange;
//        mJobDescription = JobDescription;
//    }




    public Job(@NonNull JSONObject jsonObject) throws Exception {
        mJobName = jsonObject.getString("JobName");
        mCompanyName = jsonObject.getString("CompanyName");
        mSalaryRange = jsonObject.getInt("SalaryRange");
        mJobDescription = jsonObject.getString("JobDescription");


    }

//    public JSONObject toJsonObject() {
//        JSONObject jsonProduct = new JSONObject();
//
//        try {
//            jsonProduct.put("JobName", this.getJobName());
//            jsonProduct.put("CompanyName", this.getCompanyName());
//            jsonProduct.put("SalaryRange", this.getSalaryRange());
//            jsonProduct.put("JobDescription", this.getJobDescription());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return jsonProduct;
//    }






    public int getSalaryRange() {
        return mSalaryRange;
    }

    public void setSalaryRange(int SalaryRange) {
       this.mSalaryRange = SalaryRange;
    }


    public String getJobName() {
        return mJobName;
    }

    public void setJobName(String jobName) {
        this.mJobName = jobName;
    }


    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        this.mCompanyName = companyName;
    }

    public String getJobDescription() {
        return mJobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.mJobDescription = jobDescription;
    }




    public Job(){}


}
