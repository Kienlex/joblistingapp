package com.example.kienlexyap.joblistingapp.ui;

/**
 * Created by kienlexyap on 07/09/2016.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.kienlexyap.joblistingapp.R;


public class LoginFragment extends Fragment {


    private Button mLoginButton;
    private Button mSignUpButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.mLoginButton = (Button) view.findViewById(R.id.fragment_login_button);
        this.mSignUpButton = (Button) view.findViewById(R.id.fragment_login_SignUpbutton);
        this.mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
//                LoginFragment.this.showJobListingFragment();
            }
        });

        this.mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginFragment.this.showSignUpFragment();
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }


    public void showSignUpFragment() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, new SignUpFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack("SignUp maybe")
                .commit();
    }

}
