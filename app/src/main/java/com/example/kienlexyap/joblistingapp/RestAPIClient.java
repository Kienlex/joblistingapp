package com.example.kienlexyap.joblistingapp;

import android.content.Context;
import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kienlexyap.joblistingapp.model.Employee;
import com.example.kienlexyap.joblistingapp.model.Job;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by kienlexyap on 10/10/2016.
 */

public class RestAPIClient {
    private static RestAPIClient sSharedInstance;
    private RequestQueue mRequestQueue;


    private RestAPIClient(Context context) {
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    ;

    public static synchronized RestAPIClient sharedInstance(Context context) {
        if (sSharedInstance == null) {
            sSharedInstance = new RestAPIClient(context);
        }
        return sSharedInstance;
    }

    public interface OnGetJobListingCompletedListener {
        void onComplete(@Nullable List<Job> jobs, @Nullable VolleyError error);
    }

    public void getJobListings(final OnGetJobListingCompletedListener completedListener) {
        String url = "https://api.backendless.com/v1/data/Jobs";

        JsonObjectRequest request = new BackendlessJsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<Job> jobs = new ArrayList<>();

                JSONArray dataArray = response.optJSONArray("data");
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataJson = dataArray.optJSONObject(i);
                    try {
                        Job job = new Job(dataJson);
                        jobs.add(job);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                completedListener.onComplete(jobs, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                completedListener.onComplete(null, error);
            }
        });
        mRequestQueue.add(request);
    }

    public interface OnGetUserDetailsCompleteListener {
        void onComplete(@Nullable Employee employee, @Nullable VolleyError error);
    }

    public void getUserDetails(final OnGetUserDetailsCompleteListener completeListener) {
        String url = "https://api.backendless.com/v1/data/UserParticulars";

        JsonObjectRequest request = new BackendlessJsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                List<Employee> employees = new ArrayList<>();

                JSONArray dataArray = response.optJSONArray("data");
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataJson = dataArray.optJSONObject(i);
                    try {

                        Employee employee = new Employee(dataJson);

                        completeListener.onComplete(employee, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                completeListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);

    }

    public interface oNSavedUserDetailsCompleteListener {
        void onComplete(@Nullable String employee, @Nullable VolleyError error);
    }

    public void savedUserDetails(Employee employee, final oNSavedUserDetailsCompleteListener completeListener) {
        String url = "https://api.backendless.com/v1/data/UserDetails";

        JsonObjectRequest request = new BackendlessJsonObjectRequest(Request.Method.POST, url, employee.toJsonObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String objectId = response.optString("objectID");
                completeListener.onComplete(objectId, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                completeListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }
}
