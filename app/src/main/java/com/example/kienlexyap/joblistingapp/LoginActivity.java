package com.example.kienlexyap.joblistingapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.kienlexyap.joblistingapp.ui.LoginFragment;

/**
 * Created by kienlexyap on 29/09/2016.
 */

public class LoginActivity extends AppCompatActivity{

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, new LoginFragment())
                .commit();

    }
}

