package com.example.kienlexyap.joblistingapp.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.kienlexyap.joblistingapp.MainActivity;
import com.example.kienlexyap.joblistingapp.R;
import com.example.kienlexyap.joblistingapp.RestAPIClient;
import com.example.kienlexyap.joblistingapp.model.Employee;

import java.util.concurrent.CountDownLatch;

/**
 * Created by kienlexyap on 07/09/2016.
 */
public class ProfileDetailsFragment extends Fragment {

    private TextView mFirstNameTV;
    private TextView mLastNameTV;
    private TextView mAgeTV;
    private TextView mExpectedSalaryTV;
    private TextView mExperienceTV;

    private EditText mFirstNameEdit;
    private EditText mLastNameEdit;
    private EditText mAgeEdit;
    private EditText mExpectedSalaryEdit;
    private EditText mExperienceEdit;
    private Button mUpdateButton;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_profile_details, container, false);



        mFirstNameTV = (TextView) rootView.findViewById(R.id.profileFirstNameTV);
        mLastNameTV = (TextView) rootView.findViewById(R.id.profileLastNameTV);
        mAgeTV = (TextView) rootView.findViewById(R.id.profileAgeTV);
        mExpectedSalaryTV = (TextView) rootView.findViewById(R.id.profileExpectedSalaryTV);
        mExperienceTV = (TextView) rootView.findViewById(R.id.profileExperienceTV);

        mExperienceEdit = (EditText) rootView.findViewById(R.id.profileExperienceEdit);
        mFirstNameEdit = (EditText) rootView.findViewById(R.id.profileFirstNameEdit);
        mLastNameEdit = (EditText) rootView.findViewById(R.id.profileLastNameEdit);
        mExpectedSalaryEdit = (EditText) rootView.findViewById(R.id.profileExpectedSalaryEdit);
        mAgeEdit = (EditText) rootView.findViewById(R.id.profileAgeEdit);

        this.mUpdateButton = (Button) rootView.findViewById(R.id.fragment_profile_details_UpdateButton);
        return rootView;
    }






    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {



        this.mUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Employee employee = new Employee();

                if( mExpectedSalaryEdit.getText().toString().trim().equals("") || mAgeEdit.getText().toString().trim().equals("") ||
                        mFirstNameEdit.getText().toString().trim().equals("") || mLastNameEdit.getText().toString().trim().equals("")){

                          mExpectedSalaryEdit.setError("Enter this pls");
                            mAgeEdit.setError("");
                    mLastNameEdit.setError("");
                    mFirstNameEdit.setError("");


                }

                else {

                employee.setFirstName(mFirstNameEdit.getText().toString());
                employee.setLastName(mLastNameEdit.getText().toString());
                employee.setExperience(mExperienceEdit.getText().toString());
                employee.setExpectedSalary(Integer.parseInt(mExpectedSalaryEdit.getText().toString()));
                employee.setAge(Integer.parseInt(mAgeEdit.getText().toString()));
                mFirstNameTV.setText(mFirstNameEdit.getText().toString());
                mLastNameTV.setText(mLastNameTV.getText().toString());
                mExperienceTV.setText(mExperienceEdit.getText().toString());
                mExpectedSalaryTV.setText("RM" + mExpectedSalaryEdit.getText().toString());
                mAgeTV.setText(mAgeEdit.getText().toString());
                mFirstNameEdit.getText().clear();
                mLastNameEdit.getText().clear();
                mExpectedSalaryEdit.getText().clear();
                mExperienceEdit.getText().clear();
                mAgeEdit.getText().clear();

                    RestAPIClient.sharedInstance(getActivity()).savedUserDetails(employee, new RestAPIClient.oNSavedUserDetailsCompleteListener() {
                        @Override
                        public void onComplete(@Nullable String employee, @Nullable VolleyError error) {
                            if (error != null) {
                                Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

                            } else {

                            }
                        }
                    });
                    alertDialog();

                }


            }

        });


        super.onViewCreated(view, savedInstanceState);
//        RestAPIClient.sharedInstance(getContext()).getUserDetails(new RestAPIClient.OnGetUserDetailsCompleteListener() {
//            @Override
//            public void onComplete(@Nullable Employee employee, @Nullable VolleyError error) {
//                if (error != null) {
//                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
//
//                } else {
//                        System.out.println();
//                }
//            }
//        });

       }

    public void alertDialog(){
        new AlertDialog.Builder(getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("User's Particulars are updated")
                .setMessage("Click \"Ok\" to move to Job Filter Page")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       getActivity().getSupportFragmentManager()
                               .beginTransaction()
                               .replace(R.id.activity_main_vg_container, new JobListingFragment())
                               .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                               .addToBackStack("show")
                               .commit();
                    }
                })
                .setNegativeButton("No", null)
                .show();


    }

    }

//    public void showProfileDetailFragment(){
//        getActivity().getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.activity_login_vg_container, new ProfileDetailsFragment())
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                .addToBackStack("Joblisting maybe")
//                .commit();
//    }