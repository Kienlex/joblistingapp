package com.example.kienlexyap.joblistingapp.model;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by kienlexyap on 05/10/2016.
 */

public class Employee {
    private String mObjectId;
    private int mUserId;//int or long?
    private String mPassword;
    private String mGender;
    private String mFirstName;
    private String mLastName;
    private int mAge;
    private String mEmail; //not sure whether is string or not
    private String mAddressLine1;
    private String mAddressLine2;
    private String mStreet;
    private String mCity;
    private String mCountry;
    private int mPostalCode;
    private int mExpectedSalary;
    private Calendar mCreatedDate;
    private Calendar mUpdatedDate;

    private String mExperience;


    public Employee (@NonNull JSONObject jsonObject) throws Exception {
        mFirstName = jsonObject.getString("FirstName");
        mLastName = jsonObject.getString("LastName");
        mExpectedSalary = jsonObject.getInt("ExpectedSalary");
        mAge = jsonObject.getInt("Age");
        mExperience = jsonObject.getString("Experience");
    }

    public Employee() {

    }

    public String getObjectId() {
        return mObjectId;
    }

    public void setObjectId(String objectId) {
        mObjectId = objectId;
    }

    public String getExperience() {return mExperience;}
    public void setExperience(String experience) {mExperience = experience; }

    public int getAge() {
        return mAge;
    }

    public void setAge(int age) {
        mAge = age;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getAddressLine1() {
        return mAddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        mAddressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return mAddressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        mAddressLine2 = addressLine2;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public int getPostalCode() {
        return mPostalCode;
    }

    public void setPostalCode(int postalCode) {
        mPostalCode = postalCode;
    }

    public int getExpectedSalary() {
        return mExpectedSalary;
    }

    public void setExpectedSalary(int expectedSalary) {
        mExpectedSalary = expectedSalary;
    }

    public Calendar getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Calendar createdDate) {
        mCreatedDate = createdDate;
    }

    public Calendar getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(Calendar updatedDate) {
        mUpdatedDate = updatedDate;
    }

    public JSONObject toJsonObject() {
        JSONObject jsonProduct = new JSONObject();

        try {
            jsonProduct.put("FirstName", this.getFirstName());
            jsonProduct.put("LastName", this.getLastName());
            jsonProduct.put("Age", this.getAge());
            jsonProduct.put("ExpectedSalary", this.getExpectedSalary());
            jsonProduct.put("Experience", this.getExperience());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonProduct;
    }
}
//
//}mFirstName = jsonObject.getString("FirstName");
//        mLastName = jsonObject.getString("LastName");
//        mExpectedSalary = jsonObject.getInt("ExpectedSalary");
//        mEmail = jsonObject.getString("Email");
//        mObjectID = jsonObject.getString("objectId");
//        mExperience = jsonObject.getString("Experience");
