package com.example.kienlexyap.joblistingapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.kienlexyap.joblistingapp.R;

/**
 * Created by kienlexyap on 07/09/2016.
 */
public class SignUpFragment extends Fragment {

   private Button mSignUpButton;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_signup, container, false);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        this.mSignUpButton = (Button) view.findViewById(R.id.fragment_signup_SignUpButton);

        this.mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpFragment.this.showProfileDetailFragment();
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }



    public void showProfileDetailFragment(){
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, new ProfileDetailsFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack("Joblisting maybe")
                .commit();
    }


}
