package com.example.kienlexyap.joblistingapp;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;

import com.example.kienlexyap.joblistingapp.ui.JobListingFragment;
import com.example.kienlexyap.joblistingapp.ui.JobSearchFragment;
import com.example.kienlexyap.joblistingapp.ui.LoginFragment;
import com.example.kienlexyap.joblistingapp.ui.NotificationFragment;
import com.example.kienlexyap.joblistingapp.ui.ProfileDetailsFragment;
import com.example.kienlexyap.joblistingapp.ui.SavedJobsFragment;
import com.example.kienlexyap.joblistingapp.ui.UserApplicationFragment;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
private static final int LOGIN_ACTIVITY_REQUEST_CODE = 1;
        private DrawerLayout mDrawerLayout;
        private NavigationView mNavigationView;
        private Toolbar mToolbar;
        private FloatingActionButton mFloatingActionButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.activity_main_navigation_view);
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);


//        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.fab); -- disable for the time being
//
//        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // Click action
//                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                startActivity(intent);
//            }
//        });




        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.yellowgreen));
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,mToolbar,R.string.open_drawer,R.string.close_drawer);
        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        mNavigationView.inflateMenu(R.menu.navigation_items);
        mNavigationView.setNavigationItemSelectedListener(this);

        //to hide login
        Menu menuNav=mNavigationView.getMenu();
        MenuItem nav_item2 = menuNav.findItem(R.id.action_login);
        nav_item2.setVisible(false);

}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LOGIN_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                showJobListingFragment();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivityForResult(intent, LOGIN_ACTIVITY_REQUEST_CODE);
            return true;
        }

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        changeFragment(id);

        mDrawerLayout.closeDrawers();

        return false;
    }

    private void changeFragment(int id){
        Fragment displayFragment = null;
        String title= "";


        switch (id) {
            case R.id.navDailyJobs:
                displayFragment = new JobListingFragment();
                title = "Daily Job Listing";
                break;
            case R.id.navSearchJobs:
                displayFragment = new JobSearchFragment();
                title = "Job Search";
                break;
            case R.id.navSavedJobs:
                displayFragment = new SavedJobsFragment();
                title= "Saved Jobs";
                break;
            case R.id.navUserProfiles:
                displayFragment = new ProfileDetailsFragment();
                title = "My Profile";
                break;
            case R.id.navUserApplications:
                displayFragment = new UserApplicationFragment();
                title ="My Job Applications";
                break;
            case R.id.navNotifications:
                displayFragment = new NotificationFragment();
                title ="Notifications";
                break;
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, displayFragment)
                .addToBackStack(displayFragment.getClass().getSimpleName())
                .commit();
        getSupportActionBar().setTitle(title);

    }

    private void loginMenu(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, new LoginFragment())
                  .commit();
    }

    public void showJobListingFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, new JobListingFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack("Joblisting maybe")
                .commit();
    }

}
